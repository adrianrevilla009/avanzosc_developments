
from odoo import http
from odoo.http import request

class CoasContacts(http.Controller):

    @http.route(['/contacts', '/contacts/<int:kid_id>', '/contacts/all'], type='http', auth="user",
                website=True)
    def contacts(self, kid_id=None):
        kid_ids = request.env.user.partner_id.progenitor_child_ids
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        values = {'kids': kid_ids,}
        if kid_id == None:
            report_values, number_rows_values, number_report_values = [], [], []
            total_reports = 0
            for kid in values['kids']:
                student_report_ids = []
                student_report_ids += request.env['education.group.student.report'].search([('student_id','=',kid.id),('academic_year_id', 'in', current_academic_year.ids)])
                total_reports += len(student_report_ids)
                r_number = len(student_report_ids) / 4
                if (not r_number.is_integer()):
                    r_number = int(r_number) + 1;
                number_report_values.append(len(student_report_ids))
                report_values.append(student_report_ids)
                number_rows_values.append(int(r_number))
    
            values.update({'report_ids':report_values,
                           'n_rows': number_rows_values,
                           'n_reports': number_report_values,
                           'total_reports': total_reports,})
            return http.request.render('coas_education_website_contacts.coas_contacts_layout', values)
        else:
            student_report_ids = []
            selected_kid_id = request.env['res.partner'].search([('id','=',kid_id)])
            student_report_ids += request.env['education.group.student.report'].search([('student_id','=',selected_kid_id.id),('academic_year_id', 'in', current_academic_year.ids)])
            r_number = len(student_report_ids) / 4
            if (not r_number.is_integer()):
                r_number = int(r_number) + 1;
                
            values.update({
                'selected_kid': selected_kid_id,
                'report_ids': student_report_ids,
                'number_rows': int(r_number),})
            return http.request.render('coas_education_website_contacts.coas_contacts_layout', values)
    
        