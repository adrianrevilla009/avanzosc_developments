
from odoo import http
from odoo.http import request
from datetime import date

class CoasTimetables(http.Controller):

    @http.route(['/timetables',
                 '/timetables/<int:kid_id>',
                 '/timetables/<int:kid_id>/calendar/<int:date_number>',
                 '/timetables/all/calendar/<int:date_number>'], type='http', auth="user",
                website=True)
    def timetables(self, kid_id=None, date_number=None):
        children = request.env.user.partner_id.progenitor_child_ids
        current_year = date.today().year
        current_month = date.today().month - 1
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        values = {'kids': children,
                  'current_year':current_year,
                  'current_month': current_month,}

        if kid_id == None:
            if date_number == None:
                return http.request.render('coas_education_website_timetables.coas_timetables_layout', values)
            else:
                timetables_ids = request.env[
            'education.group.student.timetable.report'].search([
                ('student_id', 'in', children.ids),
                ('academic_year_id', 'in', current_academic_year.ids),
            ])
                values.update({'timetables_ids':timetables_ids})
                return http.request.render('coas_education_website_timetables.coas_timetables_layout', values)
        else:
            selected_kid_id = request.env['res.partner'].browse(kid_id)
            timetables_ids = request.env[
            'education.group.student.timetable.report'].search([
                ('student_id', '=', selected_kid_id.id),
                ('academic_year_id', 'in', current_academic_year.ids),
            ])
            values.update({
                'timetables_ids': timetables_ids,
                'selected_kid': selected_kid_id,})
            return http.request.render('coas_education_website_timetables.coas_timetables_layout', values)
    
            