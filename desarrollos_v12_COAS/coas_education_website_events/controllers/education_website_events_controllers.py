
from odoo import http, fields, models
from odoo.http import request
from datetime import date, datetime

class CoasCalendarEvents(http.Controller):
    _inherit = ['mail.thread']

    @http.route(['/events/<int:kid_id>/calendar/<int:date_number>',
                 '/events/all/calendar/<int:date_number>',
                 '/events_accept/<int:kid_id>/<int:meeting_id>',
                 '/events_accept/all/<int:meeting_id>',
                 '/events_change/<int:kid_id>/<int:meeting_id>/<string:date_text>/<string:time_text>',
                 '/events_change/all/<int:meeting_id>/<string:date_text>/<string:time_text>'], type='http', auth="user",
                website=True)
    def meetings(self, kid_id=None, date_number=None, meeting_id=None, date_text=None, time_text=None):
        children = request.env.user.partner_id.progenitor_child_ids
        current_year = date.today().year
        current_month = date.today().month - 1
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        
        values = {'kids': children,
                  'current_year':current_year,
                  'current_month': current_month,}
        if meeting_id:
            event = request.env['calendar.event'].search([('id', '=', meeting_id)])
            if date_text and time_text:
                date_ = date_text.replace('_','-')
                time = time_text.replace('_',':') + ":00"
                start_date = str(event.start_datetime).split(" ")[0]
                start_time = str(event.start_datetime).split(" ")[1]
                stop_date = str(event.stop_datetime).split(" ")[0]
                stop_time = str(event.stop_datetime).split(" ")[1]
                diference = datetime(int(start_date.split("-")[0]),int(start_date.split("-")[1]),int(start_date.split("-")[2]),int(start_time.split(":")[0]),int(start_time.split(":")[1]),int(start_time.split(":")[2])) - datetime(int(stop_date.split("-")[0]),int(stop_date.split("-")[1]),int(stop_date.split("-")[2]),int(stop_time.split(":")[0]),int(stop_time.split(":")[1]),int(stop_time.split(":")[2]))
                inserted_date = datetime(int(date_.split("-")[0]),int(date_.split("-")[1]),int(date_.split("-")[2]),int(time.split(":")[0]),int(time.split(":")[1]),int(time.split(":")[2]))
                event.write({'stop_datetime': (inserted_date - diference)})
                event.write({'start_datetime': date_ + " " + time})
                msg_body_html = "<p>Meeting date changed to: "+ str(inserted_date) +"</p>"
                msg_body = "Meeting date changed to: "+ str(inserted_date)
            else:
                event.write({'state':'open'})
                msg_body_html = "<p>Meeting confirmed !</p>"
                msg_body = "Meeting confirmed !"
            event.message_post(body=msg_body_html)
            mail_pool = request.env['mail.mail']
            for follower in event.message_follower_ids:
                mail_values={
                        'subject': 'Calendar event change',
                        'email_to': follower.partner_id.email,
                        'body_html': msg_body_html,
                        'model': 'calendar.event',
                        'res_id': event.id,
                        'body': msg_body}
                msg_id = mail_pool.create(mail_values)
                if msg_id:
                    mail_pool.send([msg_id])

        if kid_id == None:
            education_event_ids = request.env['calendar.event'].search([('is_published','=',True),('supervised_year_id.school_year_id', 'in', current_academic_year.ids),('student_id','in',values['kids'].ids)])
            values.update({'education_event_ids':education_event_ids})
            return http.request.render('coas_education_website_events.coas_events_layout', values)
        else:
            kid = request.env['res.partner'].browse(kid_id)
            education_event_ids = request.env['calendar.event'].search([('is_published','=',True),('supervised_year_id.school_year_id', 'in', current_academic_year.ids),('student_id','=',kid.id)])
            values.update({
                'selected_kid': kid,
                'education_event_ids':education_event_ids,})
            return http.request.render('coas_education_website_events.coas_events_layout', values)
    