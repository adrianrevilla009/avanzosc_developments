
from odoo import fields, models, api

class CalendarEvents(models.Model):
    _inherit='calendar.event'
    
    is_published = fields.Boolean(string="Is published", default=False)
    
    @api.multi
    def calendar_event_publish_button(self):
        for event in self:
            event.is_published = True
    
    @api.multi
    def calendar_event_unpublish_button(self):
        for event in self:
            event.is_published = False
    
    @api.multi
    def website_publish_shortcut(self):
        for event in self:
            print(event.is_published)
        