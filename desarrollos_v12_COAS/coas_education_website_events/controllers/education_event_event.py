
from odoo import fields, models

class EventEvents(models.Model):
    _inherit='event.event'
    
    level_id = fields.Many2one(
        string='Education level',
        comodel_name='education.level')
    course_id = fields.Many2one(
        string='Education course',
        comodel_name='education.course')
    group_id = fields.Many2one(
        string='Education group',
        comodel_name='education.group')