# Copyright 2019 Adrian Revilla - AvanzOSC
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Education Website (Events) - Coas",
    "version": "12.0.1.0.0",
    "category": "",
    "license": "AGPL-3",
    "author": "AvanzOSC",
    "website": "http://www.avanzosc.es",
    "contributors": [
        "Adrian Revilla <adrianrevilla@avanzosc.es>",
        "Ana Juaristi <anajuaristi@avanzosc.es>",
    ],
    "depends": [
        "portal",
        "education",
        "website",
        "coas_education_website_main",
    ],
    "data": [
        "views/education_website_events_template.xml",
        "views/education_event_event_template.xml",
        "views/education_calendar_event_template.xml",
    ],
    "installable": True,
}
