# Copyright 2019 Adrian Revilla - AvanzOSC
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Education Website (Main) - Coas",
    "version": "12.0.1.0.0",
    "category": "",
    "license": "AGPL-3",
    "author": "AvanzOSC",
    "website": "http://www.avanzosc.es",
    "contributors": [
        "Adrian Revilla <adrianrevilla@avanzosc.es>",
        "Ana Juaristi <anajuaristi@avanzosc.es>",
    ],
    "depends": [
        "portal",
        "education",
        "website",
        "crm_school",
        "contract_school",
        "sale_school", 
        "website_event",
    ],
    "data": [
        "views/education_website_main_template.xml",
        "security/ir.model.access.csv",
    ],
    "installable": True,
}
