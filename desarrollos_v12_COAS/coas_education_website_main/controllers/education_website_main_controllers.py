
from odoo import fields, http
from odoo.http import request

class CoasMain(http.Controller):

    @http.route(['/main', '/main/<int:kid_id>'], type='http', auth="user",
                website=True)
    def main(self, kid_id=None):
        today = fields.Date.today()
        current_year = today.year
        current_month = today.month - 1
        
        children = request.env.user.partner_id.progenitor_child_ids
        values = {
            'kids': children,
            'current_year': current_year,
            'current_month': current_month,
            'number_of_kids': len(children),
        }
        if kid_id:
            kid = request.env['res.partner'].browse(kid_id)
            values['selected_kid'] = kid
            values.update(self.get_kid_values(kid))
        else:
            values.update(self.get_kid_values(children))
        return http.request.render(
            'coas_education_website_main.coas_main_layout', values)
    
    def get_kid_values(self, children):
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        # califications / academic report
        records_count = request.env['education.record'].search_count([
            ('student_id', 'in', children.ids),
            ('academic_year_id', 'in', current_academic_year.ids),
        ])
        # timetables / classes
        timetables_count = request.env[
            'education.group.student.timetable.report'].search_count([
                ('student_id', 'in', children.ids),
                ('academic_year_id', 'in', current_academic_year.ids),
            ])
        # teachers
        student_reports = request.env[
            'education.group.student.report'].search([
                ('student_id', 'in', children.ids),
                ('academic_year_id', 'in', current_academic_year.ids),
            ])
        contacts_count = len(student_reports.mapped("teacher_id"))
        # exams and homeworks
        record_exams_count = request.env['education.record'].search_count([
            ('student_id', 'in', children.ids),
            ('exam_id', '!=', False),
            ('date', '>=', fields.Date.today()),
        ])
        record_tasks_count = request.env[
            'education.group.homework.report'].search_count([
                ('student_id', 'in', children.ids),
                ('academic_year_id', 'in', current_academic_year.ids),
            ])
        # issues
        assistence_incidence = request.env.ref("issue_education.assistance_issue_type_master")
        issue_types = request.env['school.college.issue.type'].search([
            ('issue_type_id','=',assistence_incidence.id)])
        assistence_issue_ids = request.env['school.issue'].search([
                ('student_id','in',children.ids),
                ('school_issue_type_id','in',issue_types.ids)])
        negative_issue_ids = request.env['school.issue'].search([
            ('student_id','in',children.ids),
            ('school_issue_type_id.issue_type_id.gravity_scale_id.gravity_scale','<=','0'),
            ('school_issue_type_id','not in',issue_types.ids)])
        positive_issue_ids = request.env['school.issue'].search([
            ('student_id','in',children.ids),
            ('school_issue_type_id.issue_type_id.gravity_scale_id.gravity_scale','>','0')])
        students = assistence_issue_ids.mapped('student_id')
        assistances_issues = request.env['school.issue']
        if students:
            for kid in students:
                assistances = assistence_issue_ids.filtered(
                    lambda z: z.student_id.id == kid.id)
                dates = assistances.mapped('issue_date')
                for date in dates:
                    assistances = assistence_issue_ids.filtered(
                        lambda z: z.student_id.id == kid.id and z.issue_date == date)
                    assistances_issues += min(assistances, key=lambda x: x.id)
        students = positive_issue_ids.mapped('student_id')
        positive_issues = request.env['school.issue']
        if students:
            for kid in students:
                assistances = positive_issue_ids.filtered(
                    lambda z: z.student_id.id == kid.id)
                dates = assistances.mapped('issue_date')
                for date in dates:
                    assistances = positive_issue_ids.filtered(
                        lambda z: z.student_id.id == kid.id and z.issue_date == date)
                    positive_issues += min(assistances, key=lambda x: x.id)
        students = negative_issue_ids.mapped('student_id')
        negative_issues = request.env['school.issue']
        if students:
            for kid in students:
                assistances = negative_issue_ids.filtered(
                    lambda z: z.student_id.id == kid.id)
                dates = assistances.mapped('issue_date')
                for date in dates:
                    assistances = negative_issue_ids.filtered(
                        lambda z: z.student_id.id == kid.id and z.issue_date == date)
                    negative_issues += min(assistances, key=lambda x: x.id)
        # events
        education_events_count = request.env['calendar.event'].search_count([('is_published','=',True),('supervised_year_id.school_year_id', 'in', current_academic_year.ids), ('student_id','in',children.ids)])
        # routes
        routes_count = request.env['fleet.route.stop.passenger'].search_count([('partner_id','in',children.ids)])
        # route issues
        route_issues_count = request.env['fleet.route.support'].search_count([('student_id','in',children.ids)])
        # invoice/orders
        invoices_count = request.env['account.invoice'].search_count([
                ('partner_id.child_ids', 'in', children.ids),
            ])
        orders_count = request.env['sale.order'].search_count([
            ('child_id', 'in', children.ids),
            ('academic_year_id', 'in', current_academic_year.ids),
            ])
        # blogs
        news_count = request.env['blog.post'].search_count([])
        #events
        events_count = request.env['event.event'].search_count([('registration_ids.partner_id','in',children.ids)])
        
        values = {
            'number_of_records': records_count,
            'number_of_timetables': timetables_count,
            'number_of_contacts': contacts_count,
            'number_of_tasks': record_tasks_count,
            'number_of_exams': record_exams_count,
            'number_of_issues': len(positive_issues) + len(negative_issues),
            'number_of_issues2': len(assistances_issues),
            'number_of_events': education_events_count,
            'number_of_routes': routes_count,
            'number_of_invoices': invoices_count,
            'number_of_orders': orders_count,
            'number_of_news': news_count,
            'number_of_events2': events_count,
            'number_of_issues3': route_issues_count,
            }
        return values