$(document).ready(function() {
    "use strict";
    var template = document.title;
    if (template.includes("Main Layout")) {
        var current_id = window.location.pathname.replace('/main/', '');
        if (Number.isInteger(parseInt(current_id, 10))) {
            var b = $('span.badge');
            b.each(function() {
                var badge_value = $(this).text().trim();
                if (badge_value == current_id) {
                    var active_a = $(this).closest('a');
                    $(active_a).addClass('active');
                }
            });
        } else {
            var number_kids = $('#left_column > ul > li').length;
            if (number_kids == 0) {
                $('#middle_column > div > h1').html("No kids found for this user");
            }
        }
    }
});

$(document).ready(function() {
    "use strict";
    var template = document.title;
    if (template.includes("Main Layout")) {
        /* Click function for mantaining selected kids color */
        $('#kid_list > li > a').click(function(e) {
            e.preventDefault();
            $('#kid_list > li > a').removeClass('active');
            $(this).addClass('active');
            var kid_id = $(this).find('span').text();
            var origin_url = window.location.origin;
            window.location.replace(origin_url.concat("/main/").concat(kid_id));
        });
        window.localStorage.setItem("nav_timetables", "calendar");
        window.localStorage.setItem("kid_array", JSON.stringify(getKidList()));
        if ($('#kid_list > li > a[class*="active"]').text()) {
        	var kid_id = $('#kid_list > li > a[class*="active"]').find('span').text().trim();
        	window.localStorage.setItem("active_kid", kid_id);
        }else{
        	window.localStorage.setItem("active_kid", "all");
        }
        window.localStorage.removeItem("event_numbers_array");
    }
});

/* Get users kids */
function getKidList() {
	var kid_ids = [];
	$('#kid_list > li > a > span').each(function() {
		kid_ids.push($(this).text().trim())
	});
	return kid_ids;
}
