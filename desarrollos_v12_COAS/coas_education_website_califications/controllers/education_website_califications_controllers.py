
from odoo import http
from odoo.http import request

class CoasCalifications(http.Controller):

    @http.route(['/califications/<int:kid_id>',
                 '/califications/all'],
                 type='http', auth="user", website=True)
    def califications(self, kid_id=None):
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        children = request.env.user.partner_id.progenitor_child_ids
        values = {'kids': children,}

        if kid_id == None:
            education_record_ids = request.env['education.record'].search([
                ('student_id', 'in', children.ids),
                ('academic_year_id', 'in', current_academic_year.ids),
                ])
            values.update({
                'education_record_ids': education_record_ids,})
            return http.request.render('coas_education_website_califications.coas_califications_layout', values)
        else:
            kid = request.env['res.partner'].browse(kid_id)
            education_record_ids = request.env['education.record'].search([
                ('student_id', '=', kid.id),
                ('academic_year_id', 'in', current_academic_year.ids),
                ])
            values.update({
                'selected_kid': kid,
                'education_record_ids': education_record_ids,})
            return http.request.render('coas_education_website_califications.coas_califications_layout', values)
