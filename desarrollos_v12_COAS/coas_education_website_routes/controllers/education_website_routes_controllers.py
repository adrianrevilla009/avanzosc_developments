
from odoo import http
from odoo.http import request

class CoasRoutes(http.Controller):

    @http.route(['/routes/<int:kid_id>',
                 '/routes/all'],
                 type='http', auth="user", website=True)
    def routes(self, kid_id=None):
        children = request.env.user.partner_id.progenitor_child_ids
        values = {'kids': children,}

        if kid_id == None:
            route_ids = request.env['fleet.route.stop.passenger'].search([('partner_id','in',children.ids)])
            values.update({'route_ids':route_ids})
            return http.request.render('coas_education_website_routes.coas_routes_layout', values)
        else:
            kid = request.env['res.partner'].browse(kid_id)
            route_ids = request.env['fleet.route.stop.passenger'].search([('partner_id','=',kid.id)])
            values.update({'route_ids':route_ids,
                           'selected_kid': kid,})
            return http.request.render('coas_education_website_routes.coas_routes_layout', values)
    
    @http.route(['/route_support/<int:kid_id>',
                 '/route_support/all'],
                 type='http', auth="user", website=True)
    def route_support(self, kid_id=None):
        children = request.env.user.partner_id.progenitor_child_ids
        values = {'kids': children,}

        if kid_id == None:
            route_support_ids = request.env['fleet.route.support'].search([('student_id','in',children.ids)])
            values.update({'route_support_ids':route_support_ids})
            return http.request.render('coas_education_website_routes.coas_route_support_layout', values)
        else:
            kid = request.env['res.partner'].browse(kid_id)
            route_support_ids = request.env['fleet.route.support'].search([('student_id','=',kid.id)])
            values.update({'route_support_ids':route_support_ids,
                           'selected_kid': kid,})
            return http.request.render('coas_education_website_routes.coas_route_support_layout', values)
        