
from odoo import fields, http
from odoo.http import request
from datetime import date

class CoasExamTasks(http.Controller):

    @http.route(['/tasks/<int:kid_id>',
                 '/tasks/all'
                 ], type='http', auth="user",
                website=True)
    def tasks(self, kid_id=None):
        current_year = date.today().year
        current_month = date.today().month - 1
        children = request.env.user.partner_id.progenitor_child_ids
        current_academic_year = request.env['education.academic_year'].search([
            ('current', '=', True),
        ])
        values = {'kids': children,
                  'current_year':current_year,
                  'current_month': current_month,}
        if kid_id == None:
            # tasks
            record_task_ids = request.env[
                'education.group.homework.report'].search([
                    ('student_id', 'in', children.ids),
                    ('academic_year_id', 'in', current_academic_year.ids),
                ])
            values.update({
                'record_task_ids': record_task_ids,})
            return http.request.render('coas_education_website_exam_tasks.coas_task_layout', values)
        else:
            selected_kid_id = request.env['res.partner'].search([('id','=',kid_id)])
            # tasks
            record_task_ids = request.env[
                'education.group.homework.report'].search([
                    ('student_id', '=', selected_kid_id.id),
                    ('academic_year_id', 'in', current_academic_year.ids),
            ])
            values.update({
                'selected_kid': selected_kid_id,
                'record_task_ids': record_task_ids,})
            return http.request.render('coas_education_website_exam_tasks.coas_task_layout', values)
    
    @http.route(['/exams/<int:kid_id>',
                 '/exams/<int:kid_id>/calendar/<int:date_number>',
                 '/exams/all/calendar/<int:date_number>'
                 ], type='http', auth="user",
                website=True)
    def exams(self, kid_id=None, date_number=None):
        current_year = date.today().year
        current_month = date.today().month - 1
        children = request.env.user.partner_id.progenitor_child_ids

        values = {'kids': children,
                  'current_year':current_year,
                  'current_month': current_month,}
        if kid_id == None:
            # exams
            record_exam_ids = request.env['education.record'].search([
                ('student_id', 'in', children.ids),
                ('exam_id', '!=', False),
                ('date', '>=', fields.Date.today()),
            ])
            values.update({
                'record_exam_ids': record_exam_ids,})
            return http.request.render('coas_education_website_exam_tasks.coas_exam_layout', values)
        else:
            selected_kid_id = request.env['res.partner'].search([('id','=',kid_id)])
            # exams
            record_exam_ids = request.env['education.record'].search([
                ('student_id', '=', selected_kid_id.id),
                ('exam_id', '!=', False),
                ('date', '>=', fields.Date.today()),
            ])
            values.update({
                'selected_kid': selected_kid_id,
                'record_exam_ids': record_exam_ids,})
            return http.request.render('coas_education_website_exam_tasks.coas_exam_layout', values)
        